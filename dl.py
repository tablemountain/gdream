from __future__ import unicode_literals
from subprocess import call
import subprocess
import youtube_dl
import os


class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')


ydl_opts = {
    'keepvideo': True,
    'outtmpl': '%(id)s.%(ext)s',
    'logger': MyLogger(),
    'progress_hooks': [my_hook],
}
with open('yt.txt') as f:
    content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    # content = [x.strip for x in content]
    for yt_url in content:
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            meta = ydl.extract_info(yt_url, download=False)
            upload_date = meta['upload_date']
            uploader = meta['uploader']
            views = meta['view_count']
            likes = meta['like_count']
            dislikes = meta['dislike_count']
            id = meta['id']
            format = meta['format']
            duration = meta['duration']
            title = meta['title']
            description = meta['description']
            ext = meta['ext']
            print('upload date : %s' % (meta['upload_date']))
            print('uploader    : %s' % (meta['uploader']))
            print('views       : %d' % (meta['view_count']))
            print('likes       : %d' % (meta['like_count']))
            print('dislikes    : %d' % (meta['dislike_count']))
            print('id          : %s' % (meta['id']))
            print('format      : %s' % (meta['format']))
            print('duration    : %s' % (meta['duration']))
            print('title       : %s' % (meta['title']))
            print('description : %s' % (meta['description']))
            print('ext : %s' % (meta['ext']))
            if not os.path.exists(id):
                os.makedirs(id)
                os.chdir(id)
            #split the video to frames
            print(subprocess.Popen('ffmpeg ' +' -f image2 -i ' + id + " " + id + '/%06d.png', shell=True,
                           stdout=subprocess.PIPE).stdout.read())
            # framerate = 25
            # #join frames to video
            # print(subprocess.Popen('ffmpeg -y -r ' + str(
            #     framerate) + ' -start_number 0  -i "' + id + '/%06d.png" -c:v libx264 -pix_fmt yuv420p intermediate'  + "." + ext,
            #                shell=True, stdout=subprocess.PIPE).stdout.read())
            # #add audio to the video file 
            # # print(subprocess.Popen('ffmpeg -y -r ' + str(
            # #     framerate) + ' -start_number 0  -i "' + id + '/%06d.png" -c:v libx264 -pix_fmt yuv420p ' + id + "." + ext,
            # #                shell=True, stdout=subprocess.PIPE).stdout.read())
            # framerate = 25
            # #join frames to video
            # print(subprocess.Popen('ffmpeg -y -r ' + str(
            #     framerate) + ' -start_number 0  -i ' + id + '/%06d.png" -c:v libx264 -pix_fmt yuv420p intermediate.' + ext,
            #                shell=True, stdout=subprocess.PIPE).stdout.read())
            # #add audio to the video file 
            # # print(subprocess.Popen('ffmpeg  -i intermediate.' + ext + ' ' + id + "." + ext + " -c copy -map 0:v:0 -map 1:a:0 0shortest" + id + "." + ext,
            # #                shell=True, stdout=subprocess.PIPE).stdout.read())




